function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    if(arguments.length < 2 || typeof cb != 'function' || typeof n !='number'){
        throw new Error('Parameter is not a number! ');
    }
    let count =0
    
    function callinner(...args){
        if(count <n){
            count++
            return cb(...args)
        }else{
            return null
        }

       
    }
    return callinner()

}
// console.log(limitFunctionCallCount(cb,4))

module.exports = limitFunctionCallCount