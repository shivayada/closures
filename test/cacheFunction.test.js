const cacheFunction = require('../cacheFunction.cjs')

function cb(n){
    return (n*(n+1))/2

}
   
test ("provide the new array" ,() =>{
    
    const memo = cacheFunction(cb);

    expect(memo(10)).toStrictEqual(55)
  
    
})

