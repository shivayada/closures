function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    let count=0;
    
 
    return{increment : function(){
        count++
        return count
    }, decrement : function(){
        count--
        return count
    }
    
    }
    
}

// const counter =counterFactory()
module.exports = counterFactory
// count = counter.increment()
// console.log(counter.increment())

// console.log(counter.decrement())
// console.log(counter.increment())
// console.log(counter.increment())
// console.log(counter.increment())
// console.log(counter.increment())
