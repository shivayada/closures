function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    if( arguments.length<1 || typeof cb !='function'){
        throw new Error("hey i found error");
    }
    let cache ={}
    return function(...args) {
        let key = JSON.stringify(args);
        console.log(key)
        if (cache[key]) {
          console.log('Retrieving from cache');
          return cache[key];
        }
        else {
          console.log('Calculating result');
          let result = cb(...args);
          cache[key] = result;
          return result;
        }
    };
}

function cb(n){
    return (n*(n+1))/2

}

const memo = cacheFunction(cb);
console.log(memo(5,6,7))
console.log(memo(5,6,7))

module.exports =cacheFunction